# Milky Front-end Starter Kit

Milky is a very basic boilerplate for simple front end projects.

Opinionated (Pure sass, bourbon, neat, no compass)

It has Sass, Jade, Bourbon and Neat built-in to it. A Jekyll ready version is on the works as well and should be out soon.

As of december 2015, Milky is now es6 compatible and ready to use thanks to babel and browserify.

All you have to do is browse to the /dev folder and run (sudo) npm install and then gulp.

gulp will run all the necessary tasks and will watch any changes you make to the development files.

Everything will be outputed to the public folder.

I suck at writing documentation so feel free to correct me if you stumble upon this and find this documentation awful.

Have fun coding :)
